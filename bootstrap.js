const { MongoMemoryServer } = require('mongodb-memory-server');
const mongod = new MongoMemoryServer({ binary: { version: '4.2.0' } });
const mongoose = require('mongoose');

const User = require('./models/User');

module.exports = {
  start: async () => {
    try {
      const db_name = 'exercise';
      await mongod.start();
      const uri = await mongod.getUri();
      console.log('uri for mongo server is', uri);
      await mongoose.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      });
      const user = new User({
        name: 'TestAccount',
      });

      await user.save();

      console.log(
        'Account to link to config for installation of integration',
        await User.find({ name: 'TestAccount' }),
      );
    } catch (error) {
      console.log(error);
    }
  },
  getMongoURI: async () => {
    return await mongod.getUri();
  },
};
