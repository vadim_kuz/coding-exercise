const { UserService } = require('../services/UserService');

class UserController {
  static async install(request, response) {
    try {
      const { id, key } = request.body;
      const data = await UserService.install(id, key);
      response.status(200).send(data);
    } catch (e) {
      response.status(500).json({ message: 'Something went wrong' });
    }
  }

  static async uninstall(request, response) {
    try {
      const id = request.body.id;
      if (!id) {
        response.status(401).json({ message: 'User id not passed' });
      }
      await UserService.uninstall(id);
      response.status(200).json({ message: 'Session ended' });
    } catch (e) {
      response.status(500).json({ message: 'Something went wrong' });
    }
  }
}

module.exports = {
  UserController,
};
