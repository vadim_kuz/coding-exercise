const { ConversationService } = require('../services/ConversationService');

class ConversationController {
  static async getConversations(request, response) {
    try {
      const data = await ConversationService.getConversations(request.params.id, request.token);
      response.status(200).send(data.data);
    } catch (e) {
      response.status(500).json({ message: 'Something went wrong' });
    }
  }
}

module.exports = {
  ConversationController,
};
