const { ConversationController } = require('../controllers/ConversationController');
const auth = require('../middleware/Auth');
const checkUser = require('../middleware/CheckUser');

const express = require('express');

const conversation = express.Router();

conversation.get('/conversation', checkUser, auth, ConversationController.getConversations);

conversation.get('/conversation/:id', checkUser, auth, ConversationController.getConversations);

module.exports = { conversation };
