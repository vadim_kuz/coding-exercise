const express = require('express');
const { UserController } = require('../controllers/UserController');
const checkUser = require('../middleware/CheckUser');

const user = express.Router();

user.post('/install', checkUser, UserController.install);

user.post('/uninstall', checkUser, UserController.uninstall);

module.exports = { user };
