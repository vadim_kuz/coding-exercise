const express = require('express');
const bootstrap = require('./bootstrap');

const { conversation } = require('./routes/conversation');
const { user } = require('./routes/user');

bootstrap.start();

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(user);
app.use(conversation);

app.post('/status', (request, response) => {
  response.status(200).send('42 is the answer to everything');
});

app.listen(8081, () => {
  console.log(`Application Started on port 8081`);
});
