const User = require('../models/User');

module.exports = async (request, response, next) => {
  const user = await User.findOne({ _id: request.query.id });
  if (!user.token) {
    return response.status(401).json({ message: 'No authorization' });
  }
  request.token = user.token;
  next();
};
