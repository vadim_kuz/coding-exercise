const mongoose = require('mongoose');
const User = require('../models/User');

module.exports = async (request, response, next) => {
  let userID;

  if (request.method === 'GET') {
    userID = request.query.id;
  } else {
    userID = request.body.id;
  }

  if (!userID) {
    return response.status(400).json({ message: 'User id not passed' });
  }

  if (!mongoose.isValidObjectId(userID)) {
    return response.status(400).json({ message: 'User id has an incorrect format' });
  }

  const user = await User.findOne({ _id: userID });

  if (!user) {
    return response.status(400).json({ message: `User with id ${userID} not exist in db` });
  }

  next();
};
