const axios = require('axios');
const { EXTERNAL_URL } = require('../config/constants');
const User = require('../models/User');

class UserService {
  static async install(id, key) {
    const req = await axios.post(`${EXTERNAL_URL}/login`, { key: key });
    const data = await req.data;

    await User.findOneAndUpdate({ _id: id }, { $set: { token: data.token } });

    return data;
  }

  static async uninstall(id) {
    await User.findOneAndUpdate({ _id: id }, { $unset: { token: '' } });
  }
}

module.exports = {
  UserService,
};
