const axios = require('axios');
const { EXTERNAL_URL } = require('../config/constants');

class ConversationService {
  static async getConversations(id, token) {
    const API_URL = id ? `${EXTERNAL_URL}/conversation/${id}` : `${EXTERNAL_URL}/conversation`;

    const req = await axios.get(API_URL, {
      headers: {
        token: token,
      },
    });
    const data = await req.data;

    console.log(data);

    return data;
  }
}

module.exports = {
  ConversationService,
};
